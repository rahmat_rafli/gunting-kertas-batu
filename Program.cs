﻿// See https://aka.ms/new-console-template for more information
using System;

namespace Gunting_kertas_batu
{
    class Program
    {
        static void Main(String[] args)
        {
            Random random = new Random();
            bool playAgain = true;
            String Player;
            String Computer;
            String Answer;

            while (playAgain)
            {
                Player = "";
                Computer = "";
                Answer = "";

                while (Player != "ROCK" && Player != "PAPER" && Player != "SCISSORS")
                {
                    Console.Write("\nEnter ROCK, PAPER, or SCISSORS: ");
                    Player = Console.ReadLine();
                    Player = Player.ToUpper();
                }

                int randomNum = random.Next(1, 4);
                switch (randomNum)
                {
                    case 1:
                        Computer = "ROCK";
                        break;
                    case 2:
                        Computer = "PAPER";
                        break;
                    case 3:
                        Computer = "SCISSORS";
                        break;
                }

                Console.WriteLine("Player: " + Player);
                Console.WriteLine("Computer: " + Computer);

                switch (Player)
                {
                    case "ROCK":
                        if (Computer == "ROCK")
                        {
                            Console.WriteLine("DRAW!!!");
                        }
                        else if (Computer == "PAPER")
                        {
                            Console.WriteLine("YOU LOSE!!!");
                        }
                        else if (Computer == "SCISSORS")
                        {
                            Console.WriteLine("YOU WIN!!!");
                        }
                        break;
                    case "PAPER":
                        if (Computer == "ROCK")
                        {
                            Console.WriteLine("YOU WIN!!!");
                        }
                        else if (Computer == "PAPER")
                        {
                            Console.WriteLine("DRAW!!!");
                        }
                        else if (Computer == "SCISSORS")
                        {
                            Console.WriteLine("YOU LOSE!!!");
                        }
                        break;
                    case "SCISSORS":
                        if (Computer == "ROCK")
                        {
                            Console.WriteLine("YOU LOSE!!!");
                        }
                        else if (Computer == "PAPER")
                        {
                            Console.WriteLine("YOU WIN!!!");
                        }
                        else if (Computer == "SCISSORS")
                        {
                            Console.WriteLine("DRAW!!!");
                        }
                        break;
                }

                Console.Write("\nWould you like to play again? (Y/N): ");
                Answer = Console.ReadLine();
                Answer = Answer.ToUpper();

                if (Answer == "Y")
                {
                    playAgain = true;
                    Console.Clear();
                }
                else
                {
                    playAgain = false;
                }
            }
            Console.Clear();
            Console.WriteLine("Thank You for Gaming!!!");
            Console.WriteLine("Game Over....");
        }
    }
}
