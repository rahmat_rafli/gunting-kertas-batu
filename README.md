# Aplikasi Gunting Kertas Batu menggunakan C#

## Cara Buat Project Baru C# di terminal:

1. Ketik `dotnet new console` lalu tekan enter

## Cara Running C# di terminal:

1. Ketik `dotnet restore` untuk meng-update lalu tekan enter.
2. Setelah itu ketik `dotnet run` untuk menjalankan program.
